//package appstore.massiv.com.dumdumdrum;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.SharedPreferences.Editor;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Bundle;
//import android.os.Environment;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RatingBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.dropbox.client2.DropboxAPI;
//import com.dropbox.client2.android.AndroidAuthSession;
//import com.dropbox.client2.session.AccessTokenPair;
//import com.dropbox.client2.session.AppKeyPair;
//import com.dropbox.client2.session.Session.AccessType;
//import com.dropbox.client2.session.TokenPair;
//
//import java.io.File;
//import java.util.concurrent.ExecutionException;
//
//public class WocketDrummerShareGameplay extends Activity {
//	private int score;
//	private TextView tvScore;
//	private RatingBar rbScore;
//	AndroidAuthSession session;
//
//	final static private String APP_KEY = "sdtbxg0wromzkpg";
//	final static private String APP_SECRET = "g9a2oruamt6vv3x";
//
//	// If you'd like to change the access type to the full Dropbox instead of
//	// an app folder, change this value.
//	final static private AccessType ACCESS_TYPE = AccessType.APP_FOLDER;
//
//	// You don't need to change these, leave them alone.
//	final static private String ACCOUNT_PREFS_NAME = "prefs";
//	final static private String ACCESS_KEY_NAME = "ACCESS_KEY";
//	final static private String ACCESS_SECRET_NAME = "ACCESS_SECRET";
//
//	DropboxAPI<AndroidAuthSession> mApi;
//
//	private final String PHOTO_DIR = "/Musics/";
//	private ConnectivityManager connectivityManager;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_wocket_drummer_share_game_play);
//		connectivityManager = (ConnectivityManager)
//				getSystemService(Context.CONNECTIVITY_SERVICE);
//		score = getIntent().getIntExtra(
//				WocketDrummerConfig.WOCKET_DRUMMER_SCORE, 0);
//		tvScore = (TextView) findViewById(R.id.wocket_drummer_sharing_score_text);
//		rbScore = (RatingBar) findViewById(R.id.wocket_drummer_sharing_scorebar);
//		tvScore.setText("" + score);
//		rbScore.setRating((getStarsNum()));
//		session = buildSession();
//		mApi = new DropboxAPI<AndroidAuthSession>(session);
//
//		if (!mApi.getSession().isLinked()) {
//			// Start the remote authentication
//			mApi.getSession().startAuthentication(this);
//		}
//	}
//	private int getStarsNum(){
//		if(score > 1000){
//			return 3;
//		}
//		if (score < 200) {
//			return 1;
//		}
//		return 2;
//	}
//
//	public void startGame(View v) {
//		Intent i = new Intent(this, WocketDrummerLanding.class);
//		finish();
//		startActivity(i);
//	}
//
//	public void replaySession(View v) {
//		MusicWocketDrummer.playRecord();
//	}
//	public void exit(View v){
//		finish();
//	}
//
//	// checks if network is connected
//	public boolean networkConnected() {
//		NetworkInfo networkInfo =
//				connectivityManager.getActiveNetworkInfo();
//		if (networkInfo == null) {
//			return false;
//		} else
//			return true;
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		if(!networkConnected()){
//			((Button)findViewById(R.id.wocket_drummer_share_gameplay_share_button))
//			.setVisibility(View.GONE);
//			((EditText)findViewById(R.id.wocket_drummer_share_gameplay_message_to_share))
//			.setVisibility(View.GONE);
//			((TextView)findViewById(R.id.wocket_drummer_share_gameplay_text))
//			.setText("Unable to Share at this moment\nPlease Check your network connection");
//		}
//		else{
//			((Button)findViewById(R.id.wocket_drummer_share_gameplay_share_button))
//			.setVisibility(View.VISIBLE);
//			((EditText)findViewById(R.id.wocket_drummer_share_gameplay_message_to_share))
//			.setVisibility(View.VISIBLE);
//			((TextView)findViewById(R.id.wocket_drummer_share_gameplay_text))
//			.setText("Share your Performance");
//		}
//		if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
//			((Button)findViewById(R.id.button1)).setVisibility(View.GONE);
//			((Button)findViewById(R.id.wocket_drummer_share_gameplay_share_button)).setVisibility(View.GONE);
//
//		}
//		else{
//			((Button)findViewById(R.id.wocket_drummer_share_gameplay_share_button)).setVisibility(View.VISIBLE);
//			((Button)findViewById(R.id.button1)).setVisibility(View.VISIBLE);
//		}
//		super.onResume();
//
//	}
//
//	public void shareIt(View v) {
//		if (session.authenticationSuccessful()) {
//			try {
//				// Mandatory call to complete the auth
//				session.finishAuthentication();
//
//				// Store it locally in our app for later use
//				TokenPair tokens = session.getAccessTokenPair();
//				storeKeys(tokens.key, tokens.secret);
//			} catch (IllegalStateException e) {
//				showToast("Couldn't authenticate with Dropbox:"
//						+ e.getLocalizedMessage());
//			}
//		}
//		File file = new File(MusicWocketDrummer.getRecordPath());
//		WocketDrummerUpload upload = new WocketDrummerUpload(this, mApi,
//				PHOTO_DIR, file);
//		String s = "";
//		try {
//			s = upload.execute().get();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		String editTextStr = ((EditText)findViewById(R.id.wocket_drummer_share_gameplay_message_to_share)).getText().toString();
//		String message = "I got " + score + " points in Wocket dru\\m/\\m/er !"
//				+" With " + getStarsNum() + " stars !" + " Here is the link -> "+s+" ! See How I Rock !!"
//				+ "\n==========================\n" + editTextStr;
//		Intent share = new Intent(Intent.ACTION_SEND);
//		share.setType("text/plain");
//		share.putExtra(Intent.EXTRA_TEXT, message);
//
//		startActivity(Intent.createChooser(share,
//				"Sharing your performence and music using:"));
//
//	}
//
//	private AndroidAuthSession buildSession() {
//		AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);
//		AndroidAuthSession session;
//
//		String[] stored = getKeys();
//		if (stored != null) {
//			AccessTokenPair accessToken = new AccessTokenPair(stored[0],
//					stored[1]);
//			session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE,
//					accessToken);
//		} else {
//			session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE);
//		}
//
//		return session;
//	}
//
//	private String[] getKeys() {
//		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
//		String key = prefs.getString(ACCESS_KEY_NAME, null);
//		String secret = prefs.getString(ACCESS_SECRET_NAME, null);
//		if (key != null && secret != null) {
//			String[] ret = new String[2];
//			ret[0] = key;
//			ret[1] = secret;
//			return ret;
//		} else {
//			return null;
//		}
//	}
//	@Override
//	protected void onPause() {
//		super.onPause();
//		MusicWocketDrummer.stopPlaying();
//	}
//	private void showToast(String msg) {
//		Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
//		error.show();
//	}
//
//	private void storeKeys(String key, String secret) {
//		// Save the access key for later
//		SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
//		Editor edit = prefs.edit();
//		edit.putString(ACCESS_KEY_NAME, key);
//		edit.putString(ACCESS_SECRET_NAME, secret);
//		edit.commit();
//	}
//}
//
