package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher.ViewFactory;

public class WocketDrummerTutorial extends Activity {

		private ImageSwitcher imageSwitcher;
		Button btnNext;

		// Array of Image IDs to Show In ImageSwitcher
		int imageIds[] = { R.drawable.p1, R.drawable.p2, R.drawable.p3, R.drawable.p4,
				R.drawable.p5, R.drawable.p6,R.drawable.p7, R.drawable.p8, R.drawable.p9,
				R.drawable.p10, R.drawable.p11, R.drawable.p12, R.drawable.p13, R.drawable.p14,
				R.drawable.p15, R.drawable.p16, R.drawable.p17};
		int messageCount = imageIds.length;
		// to keep current Index of ImageID array
		int currentIndex = 0;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			setContentView(R.layout.activity_wocket_drummer_tutorial);

			// get The references
			btnNext = (Button) findViewById(R.id.buttonNext);
			imageSwitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher);

			// Set the ViewFactory of the ImageSwitcher that will create ImageView
			imageSwitcher.setFactory(new ViewFactory() {

				public View makeView() {
					// Create a new ImageView set it's properties
					ImageView imageView = new ImageView(getApplicationContext());
					//imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//					imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
//							LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
					return imageView;
				}
			});

			// Declare the animations and initialize them
			Animation in = AnimationUtils.loadAnimation(this,
					android.R.anim.slide_in_left);
			Animation out = AnimationUtils.loadAnimation(this,
					android.R.anim.slide_out_right);

			// set the animation type to imageSwitcher
			imageSwitcher.setInAnimation(in);
			imageSwitcher.setOutAnimation(out);
			imageSwitcher.setImageResource(imageIds[currentIndex ++]);

			// ClickListener for NEXT button
			btnNext.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					if (currentIndex < messageCount - 1)
						imageSwitcher.setImageResource(imageIds[currentIndex ++]);
					else if (currentIndex == messageCount - 1){
						imageSwitcher.setImageResource(imageIds[currentIndex ++]);
						btnNext.setText("Start New Game");
						currentIndex ++;
					}else{
						Intent startNewGame = new Intent(getBaseContext(),WocketDrummerNewGame.class);
						startActivity(startNewGame);
						finish();
					}
				}
			});

		}

	}