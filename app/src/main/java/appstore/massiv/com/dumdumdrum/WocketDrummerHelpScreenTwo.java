package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerHelpScreenTwo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_help_screen_two);
	}
	
	public void playVideo(View view){
		String url = null;
		Intent i = new Intent(Intent.ACTION_VIEW);
		switch(view.getId()){
		case R.id.wd_basic_first:
			url = new String("http://www.youtube.com/watch?v=UFTG4diUAao");
			break;
		case R.id.wd_basic4x4:
			url = new String("http://www.youtube.com/watch?v=I00o3X13gr8");
			break;		
		case R.id.wd_basic7:
			url = new String("http://www.youtube.com/watch?v=O-aD2ddAG6s");
			break;		
		}
		i.setData(Uri.parse(url));
		startActivity(i);
	}
	
	public void goBack(View view){
		Intent i = new Intent(this, WocketDrummerHelpScreenOne.class);
		startActivity(i);
		finish();
	}
	public void goNext(View view){
		Intent i = new Intent(this, WocketDrummerHelpScreenThree.class);
		startActivity(i);
		finish();
	}

}
