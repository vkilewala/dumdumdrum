package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import java.util.HashMap;

import appstore.massiv.com.dumdumdrum.R.layout;

public class WocketDrummerConfigInstruments extends Activity implements OnClickListener {

	Button prevButton;
	HashMap<Integer, Integer> buttonMusicMapping = new HashMap<Integer, Integer>();
	Animation shake;
	int wocketID;
	int soundOption;
	int soundID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		wocketID = getIntent().getIntExtra(WocketDrummerConfig.SELECTED_WOCKET,
	            WocketDrummerConfig.RIGHT_WRIST);
		soundOption = getIntent().getIntExtra(WocketDrummerConfig.WOCKET_SOUND_OPTION,0);
		String optionStr = soundOption == 0 ? "shaking" : "shaking + tapping";
		setTitle(WocketDrummerConfig.WOCKETS_NAMES[wocketID] + " Wocket " + optionStr +" Configuration");
		setContentView(R.layout.activity_wocket_drummer_config_instruments);
		shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		initBtn();
	}
	
	public void initBtn(){
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_bass, R.raw.bass_drum);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_snare, R.raw.snare);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_stick, R.raw.sticks);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_crash, R.raw.crash);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_hi_hat_open, R.raw.hihat_opened);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_hi_hat_closed, R.raw.hihat_closed);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_tom_left, R.raw.tom_left);
		buttonMusicMapping.put(R.id.wocket_drummer_config_instrument_tom_right, R.raw.tom_right);
		for(Integer anID : buttonMusicMapping.keySet()){
			Button aButton = (Button) findViewById(anID);
			aButton.setOnClickListener(this);
		}
	}
	
	public void setSound(View v){
		if (soundID != 0){
			String sounds = getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).getString(String.valueOf(wocketID), null);
			String newSounds = null;
			if (sounds == null){
				if(soundOption == 0){
					int sound1 = WocketDrummerConfig.DEFAULT_WOCKET2SOUNDS[wocketID][1];
					newSounds = Utils.parameter2string(new int[]{soundID, sound1});
				}else{
					int sound0 = WocketDrummerConfig.DEFAULT_WOCKET2SOUNDS[wocketID][0];
					newSounds = Utils.parameter2string(new int[]{sound0, soundID});
				}
			}else{
				int[] soundIDs = Utils.string2parameter(sounds);
				if(soundOption == 0){
					newSounds = Utils.parameter2string(new int[]{soundID, soundIDs[1]});
				}else{
					newSounds = Utils.parameter2string(new int[]{soundIDs[0], soundID});
				}
			}
			getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).edit().putString(String.valueOf(wocketID),newSounds).commit();
		}
		finish();
			
	}

	@Override	
	public void onClick(View button) {
		soundID = buttonMusicMapping.get(button.getId());
		MusicWocketDrummer.playForConfig(getBaseContext(), soundID);
		if(prevButton != null){
			prevButton.clearAnimation();
		}
		prevButton = (Button) button;
		button.startAnimation(shake);
	}
}
