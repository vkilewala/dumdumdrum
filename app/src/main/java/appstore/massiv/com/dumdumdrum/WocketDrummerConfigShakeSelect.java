package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class WocketDrummerConfigShakeSelect extends Activity{
	
	int wocketID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_config_shake_select);
		
		wocketID = getIntent().getIntExtra(WocketDrummerConfig.SELECTED_WOCKET,
	            WocketDrummerConfig.RIGHT_WRIST);
		setTitle(WocketDrummerConfig.WOCKETS_NAMES[wocketID] + " Wocket Configuration");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wocket_drummer_config_shake_select,
				menu);
		return true;
	}

	public void startInstruActivity(View v) {
		Intent instruIntent = new Intent(this,WocketDrummerConfigInstruments.class);
		switch (v.getId()){
			case R.id.btn_wocket_drummer_shake:
				instruIntent.putExtra(WocketDrummerConfig.WOCKET_SOUND_OPTION, 0);
				break;
			case R.id.btn_wocket_drummer_shake_tap:
				instruIntent.putExtra(WocketDrummerConfig.WOCKET_SOUND_OPTION, 1);
				break;
		}
		instruIntent.putExtra(WocketDrummerConfig.SELECTED_WOCKET, wocketID);
		startActivity(instruIntent);
		
	}
	
	public void goBack(View v){
		finish();
	}

}
