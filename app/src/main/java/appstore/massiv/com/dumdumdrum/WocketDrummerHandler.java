package appstore.massiv.com.dumdumdrum;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import appstore.massiv.com.dumdumdrum.*;

public class WocketDrummerHandler extends Handler {
	
	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";
	
	private Context context;
	private static float shakeThresholdGravity = 20.0F;
	private static int shakeTimeDifference = 200;
	private static boolean setBase[] = {true,true,true};
	private static float bases[] = {0f, 0f, 0f};
	private static int axis[] = {0, 0 ,0};
	private long mShakeTimestamp;
	private float lastBeats[] = {0f, 0f, 0f};
	private int[][] wocket2sounds;

	
	public WocketDrummerHandler(Context ctext){
		context = ctext;
		this.wocket2sounds = ((WocketDrummerLanding)context).getWocket2SoundsMapping();
	}
	
	
	
	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case MESSAGE_READ:
			byte[] readBuf = (byte[]) msg.obj;
			// construct a string from the valid bytes in the buffer
			float[] fs = Utils.bytes2floats(readBuf);
			//  String readMessage = new String(readBuf, 0, msg.arg1);
			int id = msg.arg2;
			onSensorChanged(id, fs);
//			System.out.println(fs[0]+"," + fs[1] +","+ fs[2]+" from id : "+id);
			//	System.out.println(readMessage);
			break;
		case MESSAGE_STATE_CHANGE:
			break;
		case MESSAGE_TOAST:
//			WocketDrummerNewGame.setWocketCount();
			Toast.makeText(context,
					msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
					.show();
			break;
		}
	}
	
	public void onSensorChanged(int id, float[] values) {

		if(initializeSensor(id, values) || isInvalidShake(id, values)){
			return;
		}		
		
		if (lastBeats[id]*values[axis[id]] < 0){
			lastBeats[id] = 0f;
			return;
		}
		

		Float gForce = Math.abs(values[axis[id]]-bases[id]);
//		System.out.println(gForce);
		

		if (gForce > shakeThresholdGravity) {
			final long now = System.currentTimeMillis();
			// ignore shake events too close to each other (500ms)
			if (mShakeTimestamp + shakeTimeDifference > now) {
				return;
			}

			mShakeTimestamp = now;
			if (lastBeats[id] == 0f) {
				boolean pressed = ((WocketDrummerLanding) context).pressed;
				int musicID = wocket2sounds[id][pressed ? 1 : 0];
				MusicWocketDrummer.play(musicID);
				((WocketDrummerLanding)context).shakeCounterAddOne();
				lastBeats[id] = values[axis[id]];
			}
		}
	}
	
	private boolean isInvalidShake(int id,float[] values) {
		return values[axis[id]]*bases[id] > 0 && Math.abs(values[axis[id]]) > Math.abs(bases[id]);
	}

	private boolean initializeSensor(int id, float[] values) {
		boolean result = false;
		if (setBase[id]) {
			axis[id] = parse(values);
			bases[id] = values[axis[id]];
			setBase[id] = false;
			result = true;
			System.out.println("Initializing along  : "+axis[id]+"For ID : "+id);
			System.out.println("Values : "+values[0]+" - "+values[1]+" - "+values[2]);
		}		
		return result;
	}

	public int parse(float[] v){
		int result;
		result = (Math.abs(v[0]) > Math.abs(v[1])) ? 0 : 1;

		result = (Math.abs(v[result]) > Math.abs(v[2])) ? result : 2;
		return result;
	}

	public void setBase() {
		setBase = new boolean[]{true,true,true};
	}
}
