package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerShowHelp extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_show_help);
	}

	// startResources
	public void startResources(View view){
		Intent resources = new Intent(this,WocketDrummerHelpScreenOne.class);
		startActivity(resources);
		finish();
	}

	// startTutorial
	public void startTutorial(View view){
		Intent i = new Intent(this, WocketDrummerTutorial.class);
		startActivity(i);
	}
	
	public void showOwnVideo(View view){
		String url = new String("http://youtu.be/_PGUeJK15Ko");
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}
}
