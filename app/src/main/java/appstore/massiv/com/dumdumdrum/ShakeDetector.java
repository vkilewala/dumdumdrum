package appstore.massiv.com.dumdumdrum;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class ShakeDetector implements SensorEventListener {

	/*
	 * The gForce that is necessary to register as shake.
	 * Must be greater than 1G (one earth gravity unit).
	 * You can install "G-Force", by Blake La Pierre
	 * from the Google Play Store and run it to see how
	 *  many G's it takes to register a shake
	 */
	private static float shakeThresholdGravity = 15.0F;
	private static int shakeTimeDifference = 150;
	private static boolean setBase = true;
	private static float base = 0f;
	private static int i = 0;
	private OnShakeListener mListener;
	private long mShakeTimestamp;
//	private int mShakeCount;
	private float lastBeat = 0f;
	
	
	public static float getShakeThresholdGravity() {
		return shakeThresholdGravity;
	}


	public static void setShakeThresholdGravity(float shakeThresholdGravity) {
		ShakeDetector.shakeThresholdGravity = shakeThresholdGravity;
	}


	public static int getShakeTimeDifference() {
		return shakeTimeDifference;
	}


	public static void setShakeTimeDifference(int shakeTimeDifference) {
		ShakeDetector.shakeTimeDifference = shakeTimeDifference;
	}

    public void setOnShakeListener(OnShakeListener onShakeListener) {
		this.mListener = onShakeListener;
	}


	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// ignore
	}

	public int parse(float[] v){
		int result;
		result = (Math.abs(v[0]) > Math.abs(v[1])) ? 0 : 1;

		result = (Math.abs(v[result]) > Math.abs(v[2])) ? result : 2;
		return result;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		if(initializeSensor(event) || isInvalidShake(event)){
			return;
		}
		
		
//		WocketDrummerNewGame.wdBluetoothService.write(Utils.floats2bytes(event.values));
//		WocketDrummerNewGame.wdBluetoothService.write((event.values[0]+"_"+event.values[1]+"_"+event.values[2]).getBytes());
		
		
		if (lastBeat*event.values[i] < 0){
			lastBeat = 0f;
			return;
		}
		

		Float gForce = Math.abs(event.values[i]-base);
//		System.out.println(gForce);
		

		if (gForce > shakeThresholdGravity) {
			final long now = System.currentTimeMillis();
			//System.out.println(gForce);
			// ignore shake events too close to each other (500ms)
			if (mShakeTimestamp + shakeTimeDifference > now) {
				return;
			}

			mShakeTimestamp = now;
//			mShakeCount++;

			if (lastBeat == 0f) {
				mListener.onShake();
				lastBeat = event.values[i];
			}
		}
	}
	
	
	


	private boolean isInvalidShake(SensorEvent event) {
		return event.values[i]*base > 0 && Math.abs(event.values[i]) > Math.abs(base);
	}


	private boolean initializeSensor(SensorEvent event) {
		boolean res = false;
		if (mListener != null && setBase) {
			i = parse(event.values);
			base = event.values[i];
			setBase = false;
			res = true;
//			System.out.println("----------" + i +"=="+ event.values[0] +"==" + event.values[1] +"=="+ event.values[2]);
		}		
		return res;
	}


	public void setBase() {
		setBase = true;
	}
}


