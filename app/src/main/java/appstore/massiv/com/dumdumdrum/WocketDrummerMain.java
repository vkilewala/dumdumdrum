package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerMain extends Activity {

	protected static final String FIRST_TIME = "first_time_flag";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_main);
		WocketDrummerConfig.init();
	}

	@Override
	protected void onResume() {
		// Get Previous data
		super.onResume();
	}

	// Start new Game
	public void startNewGame(View view){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				this).setTitle("Information");

		// set dialog message
		alertDialogBuilder
		.setMessage("We have replaced wockets with phones in this application and we are giving you the option to connect your phones as wockets ! Do you want to continue ?")
		.setCancelable(false)
		.setPositiveButton("Yes ! Continue ",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				if(getPreferences(MODE_PRIVATE).getBoolean(FIRST_TIME, true)){
					getPreferences(MODE_PRIVATE).edit().putBoolean(FIRST_TIME, false).commit();
					Intent startTutes = new Intent(getBaseContext(),WocketDrummerTutorial.class);
					startActivity(startTutes);
				}
				else{
					Intent startNewGame = new Intent(getBaseContext(),WocketDrummerNewGame.class);
					startActivity(startNewGame);
				}
			}
		}).setNegativeButton("No ! Quit",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
			}
		}).show();
	}
	// Start Help
	public void startHelp(View view){
		Intent startHelp = new Intent(this,WocketDrummerShowHelp.class);
		startActivity(startHelp);

	}
	// Start Ack
	public void startAck(View view){
		Intent startAck = new Intent(this,WocketDrummerAckScreen.class);
		startActivity(startAck);
	}
	// quit Game
	public void quitGame(View view){
		finish();
	}

}
