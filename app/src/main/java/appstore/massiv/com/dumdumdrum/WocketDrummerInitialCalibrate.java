package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerInitialCalibrate extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_initial_calibrate);
	}

	public void startGameAfterCalibrate(View view){
		Intent i = new Intent();
		setResult(RESULT_OK, i);
		finish();
	}
}
