package appstore.massiv.com.dumdumdrum;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import appstore.massiv.com.dumdumdrum.R;
import appstore.massiv.com.dumdumdrum.Utils;
import appstore.massiv.com.dumdumdrum.WocketDrummerConfig;
import appstore.massiv.com.dumdumdrum.WocketDrummerHandler;
import appstore.massiv.com.dumdumdrum.WocketDrummerInitialCalibrate;
import appstore.massiv.com.dumdumdrum.WocketDrummerNewGame;
//import appstore.massiv.com.dumdumdrum.WocketDrummerShareGameplay;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder.AudioSource;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class WocketDrummerLanding extends Activity implements OnCompletionListener {

	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private ShakeDetector mShakeDetector;
	private TextView counter;

	private TextView timeSeekText;
	private TextView sensitivitySeekText;

	private Button soundToggle;

	private SeekBar timeSeekbar;
	private SeekBar sensitivitySeekbar;
	public boolean pressed;
	private CountDownTimer timer;
	private TextView gameScreenTimer;
	private int currTimeElapsed = 0;
	private int shakeCounter = 0;
	private static final int CALIBRATE_REQUEST = 108080; 


	// The Handler that gets information back from the BluetoothChatService
	private WocketDrummerHandler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWocket2SoundsMapping();
		mHandler = new WocketDrummerHandler(this);
		setContentView(R.layout.activity_wocket_drummer_landing);
		setTitle(MusicWocketDrummer.getSongName());
		
		Intent calibrateDialog = new Intent(this, WocketDrummerInitialCalibrate.class);
		startActivityForResult(calibrateDialog, CALIBRATE_REQUEST);
	
		soundToggle = (Button) findViewById(R.id.second_sound_button);
		soundToggle.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event){
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					pressed = true;
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					pressed = false;
				}

				return true;
			}

		});
		gameScreenTimer = (TextView) findViewById(R.id.timer);
		counter = (TextView) findViewById(R.id.test);
		timer = new WocketDrummerTimer(Long.MAX_VALUE, 1000);
		timer.start();
		timeSeekbar = (SeekBar) findViewById(R.id.wocketdrummer_landing_shake_interval_seekbar);
		timeSeekbar.setProgress(raw2Progress(50, 400,
				ShakeDetector.getShakeTimeDifference()));
		timeSeekText = (TextView) findViewById(R.id.wocketdrummer_landing_shake_interval_seekbar_text);

		sensitivitySeekbar = (SeekBar) findViewById(R.id.wocketdrummer_landing_accelerometer_sensitivity_seekbar);
		sensitivitySeekbar.setProgress(raw2Progress(10, 40,
				(int) ShakeDetector.getShakeThresholdGravity()));
		sensitivitySeekText = (TextView) findViewById(R.id.wocketdrummer_landing_accelerometer_sensitivity_seekbar_text);

		timeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				ShakeDetector.setShakeTimeDifference(progress2Raw(50, 400,
						seekBar.getProgress()));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				timeSeekText
				.setText(getResources()
						.getString(
								R.string.wocketdrummer_landing_shake_interval_seekbar_textview)
								+ " - " + progress2Raw(50, 400, progress));

			}
		});

		sensitivitySeekbar
		.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				ShakeDetector.setShakeThresholdGravity(progress2Raw(10,
						40, seekBar.getProgress()));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar,
					int progress, boolean fromUser) {
				sensitivitySeekText
				.setText(getResources()
						.getString(
								R.string.wocketdrummer_landing_accelerometer_sensitivity_seekbar_textview)
								+ " - "
								+ progress2Raw(10, 40, progress));

			}
		});

		// ShakeDetector initialization
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mShakeDetector = new ShakeDetector();
		mShakeDetector.setOnShakeListener(new OnShakeListener() {
			@Override
			public void onShake() {
				String defaultSounds = Utils.parameter2string(WocketDrummerConfig.DEFAULT_WOCKET2SOUNDS[3]);
				String bindingStr = getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).getString(String.valueOf(3),defaultSounds);
				int soundID = Utils.string2parameter(bindingStr)[pressed ? 1 : 0];
				shakeCounterAddOne();
				MusicWocketDrummer.play(soundID);
			}
		});
		recalibrate(null);
		Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss");
        String newMusFile = df.format(date) + ".3gp";
		MusicWocketDrummer.startRecording(newMusFile);
	}
	
	public void shakeCounterAddOne(){
		shakeCounter++;
		counter.setText(shakeCounter + "");
	}

	public int[][]  getWocket2SoundsMapping(){
		int[][] wocket2sounds = new int[4][2];
		for(int i = 0; i < 4; i++){
			String defaultSounds = Utils.parameter2string(WocketDrummerConfig.DEFAULT_WOCKET2SOUNDS[i]);
			String bindingStr = getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).getString(String.valueOf(i),defaultSounds);
			wocket2sounds[i] = Utils.string2parameter(bindingStr);
		}
		return wocket2sounds;
	}

	/**
	 * returns progress value on scale of 0-100
	 * 
	 * @param start
	 *            - minimum raw value
	 * @param end
	 *            - maximum raw value
	 * */
	private int raw2Progress(int start, int end, int rawProgress) {
		return (int) (((rawProgress - start) * 100) / (float) (end - start));

	}

	/**
	 * returns raw value on scale of 0-100
	 * 
	 * @param start
	 *            - minimum raw value
	 * @param end
	 *            - maximum raw value
	 * */
	private int progress2Raw(int start, int end, int processedProgress) {
		return (int) ((processedProgress * (end - start)) / 100f + start);
	}

	@Override
	public void onResume() {
		super.onResume();
	
		// Add the following line to register the Session Manager Listener
		// onResume
		MusicWocketDrummer.init(this,this);
		mSensorManager.registerListener(mShakeDetector, mAccelerometer,
				SensorManager.SENSOR_DELAY_FASTEST);
//		WocketDrummerNewGame.wdBluetoothService.setmHandler(mHandler);
	}

	@Override
	public void onPause() {
		// Add the following line to unregister the Sensor Manager onPause
		mSensorManager.unregisterListener(mShakeDetector);
		MusicWocketDrummer.stop(getBaseContext());
		super.onPause();
	}

	public void showWocketAck(View view){
//		Intent wocketAck = new Intent(getBaseContext(), WocketDrummerAck.class);
//		startActivity(wocketAck);
	}

	public void recalibrate(View view) {
		mHandler.setBase();
		mShakeDetector.setBase();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == CALIBRATE_REQUEST){
			if(resultCode == RESULT_OK){
				mHandler.setBase();
				mShakeDetector.setBase();
			}
		}
	}
	public void exitApp(View view) {
		finish();
	}

	public void toggleSettings(View view){
		LinearLayout advControls = ((LinearLayout)findViewById(R.id.wocket_drummer_landing_advanced_controls));
		if(advControls.getVisibility() == View.GONE){
			advControls.setVisibility(View.VISIBLE);
		}else{
			advControls.setVisibility(View.GONE);
		}
	}

	public void pauseGame(View view){
		MusicWocketDrummer.pause();
		timer.cancel();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				this).setTitle("Ga\\m/e Paused");

		// set dialog message
		alertDialogBuilder
		.setMessage("Ga\\m/e is Paused")
		.setCancelable(false)
		.setPositiveButton("Resume",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				MusicWocketDrummer.resume();
				timer = new WocketDrummerTimer(Long.MAX_VALUE, 1000);
				timer.start();
			}
		})
		.setNeutralButton("Restart",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				restartLanding();
			}
		}).setNegativeButton("Exit",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				showResult();
			}
		}).show();
	}

	// Loop on it's own
	protected void restartLanding() {
		MusicWocketDrummer.stopRecording();
		Intent i = getIntent();
		finish();
		startActivity(i);
	}

	//	show results screen
	// Share screen persay
	protected void showResult() {
//		Intent shareScreenActivity = new Intent(this, WocketDrummerShareGameplay.class);
//		MusicWocketDrummer.stopRecording();		shareScreenActivity.putExtra(WocketDrummerConfig.WOCKET_DRUMMER_SCORE, getScore());
//		startActivity(shareScreenActivity);
//		finish();
	}
	
	private int getScore(){
		return shakeCounter * 500 / currTimeElapsed;
	}

	// Listening to Media player for song finish.
	@Override
	public void onCompletion(MediaPlayer mp) {
		showResult();
	}
	
	public class WocketDrummerTimer extends CountDownTimer{

		public WocketDrummerTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
		}

		@Override
		public void onTick(long millisUntilFinished) {
			currTimeElapsed++;
			gameScreenTimer.setText(currTimeElapsed/60+":"+currTimeElapsed%60);
		}
	}
	@Override
    public void onBackPressed() { 
        pauseGame(null);
    }
}
