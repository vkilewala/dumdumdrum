package appstore.massiv.com.dumdumdrum;

import appstore.massiv.com.dumdumdrum.R;
import appstore.massiv.com.dumdumdrum.R.layout;
import appstore.massiv.com.dumdumdrum.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class FinalProjectLanding extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final_project_landing);
	}
	
	// Exits
	public void quit(View view){
		finish();
	}
	
	public void startWDDrummer(View view){
		Intent i = new Intent(this, WocketDrummerMain.class);
		startActivity(i);
		finish();
	}
}
