package appstore.massiv.com.dumdumdrum;



import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

public class WocketDrummerNewGame extends Activity {

	private BluetoothAdapter mBluetoothAdapter;
	public Spinner songsSpinner;
	public static Button buttonConnectAsWocket;
	public static Button buttonConnectAsMaster;
	public static TableLayout buttonsTable;


	private static final String TAG = "WocketsList";
	private static int numberOfConnectedWocket = 0;

	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes

	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;


	private int[] wocketID2btnID = {R.id.wocket_drummer_new_game_connect_wocket_left_hand,R.id.wocket_drummer_new_game_connect_wocket_left_leg,
			R.id.wocket_drummer_new_game_connect_wocket_right_leg, R.id.wocket_drummer_new_game_connect_wocket_right_hand};

	private int currentPartButtonId = 0;   


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_new_game);

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		buttonConnectAsMaster = (Button) findViewById
				(R.id.wocket_drummer_new_game_connect_accelerometers);
		buttonConnectAsWocket  = 
				(Button) findViewById
				(R.id.wocket_drummer_new_game_connect_as_wocket_button);
		buttonsTable =  
				(TableLayout) findViewById
				(R.id.wocket_drummer_new_game_wockets_parts_table);


//		if (mBluetoothAdapter == null) {
//			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
//			finish();
//			return;
//		}

		songsSpinner = (Spinner) findViewById(R.id.wocket_drummer_new_game_music_selection);
		ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,WocketDrummerConfig.SONG_NAMES);
		songsSpinner.setAdapter(adapter);
		songsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				MusicWocketDrummer.setBackgroundMusic(WocketDrummerConfig.SONG_IDS[arg2]);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
//		// If BT is not on, request that it be enabled.
//		// setupChat() will then be called during onActivityResult
//		if (!mBluetoothAdapter.isEnabled()) {
//			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
//			// Otherwise, setup the chat session
//		} else {
//			if (wdBluetoothService == null){
//				wdBluetoothService = new WocketDrummerBluetoothService(this, mHandler);
//			}
//		}
	}

	public synchronized void onResume() {
		super.onResume();
//		// Performing this check in onResume() covers the case in which BT was
//		// not enabled during onStart(), so we were paused to enable it...
//		// onResume() will be called when ACTION_REQUEST_ENABLE activity
//		// returns.
//		if (wdBluetoothService != null) {
//			wdBluetoothService.setmHandler(mHandler);
//			setConnectedButtons();
//			// Only if the state is STATE_NONE, do we know that we haven't
//			// started already
//			if (wdBluetoothService.getState() == wdBluetoothService.STATE_NONE) {
//				// Start the Bluetooth chat services
//				wdBluetoothService.start();
//			}
//		}
	}

	// sets connected buttons
//	private void setConnectedButtons() {
//		boolean[] ctdArray = wdBluetoothService.getConnectedArray();
//		for(int i = 0; i < ctdArray.length; i++){
//			if(!ctdArray[i]){
//				Button btn = (Button) findViewById(wocketID2btnID[i]);
//				btn.setEnabled(true);
//				btn.setTextColor(getResources().getColor(R.color.quit_button));			}
//		}
//	}

	// starts wocket config Screen
	public void startConfig(View view) {
		Intent config = new Intent(this, WocketDrummerConfigMain.class);
		startActivity(config);
	}

	// starts Master Slave selection dialog
	//				Intent listWockets = new Intent(getBaseContext(),
	//						WocketDrummerWocketsList.class);
	//				startActivityForResult(listWockets, 666);
	//				Intent wocketDiscover = new Intent(getBaseContext(),
	//						WocketDrummerWocketDiscover.class);
	//				startActivity(wocketDiscover);



	// Connect as master phone 
	// Will give the choice to configure
	// LH,LL,RL
	public void startConnectAsMaster(View view){
		view.setVisibility(View.GONE);
		buttonConnectAsWocket.setVisibility(View.GONE);
		buttonsTable.setVisibility(View.VISIBLE);
	}

	// Connect the phone as just the wocket
	public void startConnectAsWocket(View view){
		Intent wocketDiscover = new Intent(getBaseContext(),
				WocketDrummerWocketDiscover.class);
		startActivity(wocketDiscover);
	}

	// Connceting a wocket for part 
	public void startConnectWocketByPart(View view){
		currentPartButtonId = view.getId();
		Intent listWockets = new Intent(getBaseContext(),
				WocketDrummerWocketsList.class);
		startActivityForResult(listWockets, REQUEST_CONNECT_DEVICE);

	}

	public void enableBtn(int wocketID){
		if(wocketID >= 0){
			Button btn = (Button) findViewById(wocketID2btnID[wocketID]);
			btn.setEnabled(true);
			btn.setTextColor(getResources().getColor(R.color.quit_button));
		}
	}

	public void toggleBackMusicSelection(View view){
		RadioButton rb = (RadioButton) view;
		if(rb.getId() == R.id.wocket_drummer_no_music_radio){
			songsSpinner.setVisibility(View.GONE);
			MusicWocketDrummer.setBackgroundMusic(-1);
		}
		else{
			songsSpinner.setVisibility(View.VISIBLE);
			MusicWocketDrummer.setBackgroundMusic(WocketDrummerConfig.SONG_IDS[songsSpinner.getSelectedItemPosition()]);
		}
		
	}

	// returns wocket id for this part
	private int getWocketID(int id) {
		switch(id){
		case R.id.wocket_drummer_new_game_connect_wocket_left_hand:
			return WocketDrummerConfig.LEFT_WRIST;
		case R.id.wocket_drummer_new_game_connect_wocket_left_leg:
			return WocketDrummerConfig.LEFT_ANKLE;
		case R.id.wocket_drummer_new_game_connect_wocket_right_leg:
			return WocketDrummerConfig.RIGHT_ANKLE;
		case R.id.wocket_drummer_new_game_connect_wocket_right_hand:
			return WocketDrummerConfig.RIGHT_WRIST;
		default: return -1;
		}
	}

	private void makeDiscoverable() {
		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
//				connectDevice(data, getWocketID(currentPartButtonId));
			}
			break;
		case REQUEST_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up a chat session
				((LinearLayout) findViewById(R.id.wocket_drummer_new_game_multi_buttons)).setVisibility(View.VISIBLE); 
//				wdBluetoothService = new WocketDrummerBluetoothService(this, mHandler);
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BT not enabled");
				((LinearLayout) findViewById(R.id.wocket_drummer_new_game_multi_buttons)).setVisibility(View.GONE); 
				Toast.makeText(this, "Please turn on the Bluetooth to play in multiplayer mode",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				// construct a string from the valid bytes in the buffer
				float[] fs = Utils.bytes2floats(readBuf);
				//				String readMessage = new String(readBuf, 0, msg.arg1);
				//				System.out.println(fs[0]+"," + fs[1] +","+ fs[2]);
				//				System.out.println(readMessage);
				break;
			case MESSAGE_STATE_CHANGE:
				Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
//				switch (msg.arg1) {
//				case WocketDrummerBluetoothService.STATE_CONNECTED:
//					Toast.makeText(getApplicationContext(),
//							"Connected", Toast.LENGTH_SHORT)
//							.show();
//					numberOfConnectedWocket++;
//					Button partButton =
//							(Button) findViewById(currentPartButtonId);
//					if( partButton != null){
//						partButton.setEnabled(false);
//						partButton.setTextColor(getResources().getColor(R.color.green));
//					}
//					break;
//				case WocketDrummerBluetoothService.STATE_CONNECTING:
//					break;
//				}
//				break;
//			case MESSAGE_TOAST:
//				//				setWocketCount();
//				Bundle bndl = msg.getData();
//				Toast.makeText(getApplicationContext(),
//						bndl.getString(TOAST), Toast.LENGTH_SHORT)
//						.show();
//				enableBtn(bndl.getInt(WocketDrummerConfig.WOCKET_ID));
//				break;
			}
		}
	};

	private void connectDevice(Intent data, int wocketID) {
		// Get the device MAC address
		String address = data.getExtras().getString(
				WocketDrummerWocketsList.EXTRA_DEVICE_ADDRESS);
		// Get the BLuetoothDevice object
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		// Attempt to connect to the device
//		wdBluetoothService.connect(device, wocketID);
	}

	public void sendMessage(String message) {
		// Check that we're actually connected before trying anything
//		if (wdBluetoothService.getState() != WocketDrummerBluetoothService.STATE_CONNECTED) {
//			Toast.makeText(this, "not connected", Toast.LENGTH_SHORT).show();
//			return;
//		}
//		// Check that there's actually something to send
//		if (message.length() > 0) {
//			// Get the message bytes and tell the BluetoothChatService to write
//			byte[] send = message.getBytes();
//			wdBluetoothService.write(send);
//		}
	}

	public void startGame(View view){
		Intent i = new Intent(getBaseContext(), WocketDrummerLanding.class);
		startActivity(i);
	}

	public void setWocketCount() {
		if(numberOfConnectedWocket == 0){
			buttonConnectAsMaster.setVisibility(View.VISIBLE);
			buttonConnectAsWocket.setVisibility(View.VISIBLE);
			buttonsTable.setVisibility(View.GONE);
		}
	}

	public void goBack(View v){
		finish();
	}

	// reset Connection type 
	public void reset(View view){
//		wdBluetoothService.stop();
//		wdBluetoothService.start();
	}
};

