package appstore.massiv.com.dumdumdrum;

import java.io.IOException;
import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.os.Environment;
import android.util.Log;

public class MusicWocketDrummer {

	private static MediaPlayer mpBackGround;
	private static SoundPool soundPool;
	private static MediaRecorder mRecorder;

	private static int backgroundMusicID = -1;
	private static HashMap<Integer, Integer> musicID2poolID;
	private static String DIR = Environment.getExternalStorageDirectory()
			.getAbsolutePath() + "/";
	private static String path;

	public static void init(Context context, OnCompletionListener listener) {
		musicID2poolID = new HashMap<Integer, Integer>();
		soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
		for (int soundID : WocketDrummerConfig.ALL_SOUNDS) {
			musicID2poolID.put(soundID, soundPool.load(context, soundID, 1));
		}

		if (backgroundMusicID == -1) {
			return;
		}
		mpBackGround = MediaPlayer.create(context, backgroundMusicID);
		mpBackGround.setVolume(1f, 1f);
		mpBackGround.setLooping(false);
		mpBackGround.setOnCompletionListener(listener);
		mpBackGround.start();
	}

	public static String getSongName() {
		if (backgroundMusicID == -1) {
			return "No Music";
		}
		int[] songIDs = WocketDrummerConfig.SONG_IDS;
		int i = 0;
		for (; i < songIDs.length; i++) {
			if (backgroundMusicID == songIDs[i]) {
				return WocketDrummerConfig.SONG_NAMES[i];
			}
		}
		return "No Music";
	}

	/** Stop old song and start new one */

	public static void play(int id) {
		soundPool.play(musicID2poolID.get(id), 1f, 1f, 1, 0, 1f);
	}

	public static void playForConfig(Context context, int resID) {
		stopForConfig();
		mpBackGround = MediaPlayer.create(context, resID);
		mpBackGround.start();
	}

	public static void stopForConfig() {
		if (mpBackGround != null) {
			mpBackGround.stop();
			mpBackGround.release();
			mpBackGround = null;
		}
	}

	/** Stop the music */
	public static void stop(Context context) {
		if (mpBackGround != null) {
			mpBackGround.stop();
			mpBackGround.release();
			mpBackGround = null;
		}
	}

	public static void pause() {
		if (mpBackGround != null) {
			mpBackGround.pause();
		}
	}

	public static void resume() {
		if ((mpBackGround != null) && (!mpBackGround.isPlaying())) {
			mpBackGround.start();
		}
	}

	public static void setBackgroundMusic(int i) {
		backgroundMusicID = i;
	}

	public static void startRecording(String file) {
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			mRecorder = new MediaRecorder();
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			mRecorder.setAudioEncodingBitRate(32);
			mRecorder.setAudioSamplingRate(44100);
			path = DIR + file;
			mRecorder.setOutputFile(path);

			try {
				mRecorder.prepare();
			} catch (IOException e) {
				System.out.println("Audio record prepare failed");
			}
			mRecorder.start();
		}
	}

	public static void stopRecording() {
		if (mRecorder != null) {
			mRecorder.stop();
			mRecorder.release();
			mRecorder = null;
		}
	}

	public static void playRecord() {
		mpBackGround = new MediaPlayer();
		try {
			mpBackGround.setDataSource(path);
			mpBackGround.prepare();
			mpBackGround.start();
		} catch (IOException e) {
			Log.e("music", "prepare() failed");
		}
	}

	public static void stopPlaying() {
		if (mpBackGround != null) {
			mpBackGround.release();
			mpBackGround = null;
		}
	}
	public static String getRecordPath(){
		return path;
	}
}
