package appstore.massiv.com.dumdumdrum;

import java.util.HashMap;


public class WocketDrummerConfig {
	public final static int RIGHT_WRIST = 3;
	public final static int LEFT_WRIST = 0;
	public final static int LEFT_ANKLE = 1;
	public final static int RIGHT_ANKLE = 2;
	
	public final static int[][] DEFAULT_WOCKET2SOUNDS = new int [4][2];
	
	public final static String[] WOCKETS_NAMES = {"Left Wrist", "Left Ankle", "Right Ankle", "Right Wrist"};
	
	public final static String SELECTED_WOCKET = "selected_wocket";
	public final static String WOCKET_SOUND_OPTION = "wocket_sound_option";
	public final static String WOCKET_ID = "wktid";
	public final static String WOCKET_DRUMMER_SCORE = "wocket_drummer_score";
	public final static String SHARE_PREFERENCE_NAME = "neu.edu.madcourse.wocketdrummer";
	public final static int[] ALL_SOUNDS = {
		R.raw.tom_left, R.raw.tom_right, 
		R.raw.snare, R.raw.sticks, 
		R.raw.hihat_opened, R.raw.hihat_closed, 
		R.raw.bass_drum,R.raw.crash};
	
	public final static int SONG_IDS[] = {
			R.raw.rhcp_californication,
			R.raw.rhcp_snow,
			R.raw.soad_aerials,
			R.raw.we_will_rock_you
	};

	public final static String SONG_NAMES[] = {
			"Californication",
			"Snow",
			"Aerials",
			"We will rock you"
	};
	
	public final static HashMap<Integer, Integer> sound2ImageMap = new HashMap<Integer, Integer>();
	
	public static void init(){
		DEFAULT_WOCKET2SOUNDS[LEFT_WRIST] = new int[]{R.raw.tom_left, R.raw.crash};
		DEFAULT_WOCKET2SOUNDS[RIGHT_WRIST] = new int[]{R.raw.tom_right, R.raw.sticks};
		DEFAULT_WOCKET2SOUNDS[LEFT_ANKLE] = new int[]{R.raw.hihat_opened, R.raw.hihat_closed};
		DEFAULT_WOCKET2SOUNDS[RIGHT_ANKLE] = new int[]{R.raw.bass_drum, R.raw.snare};
		
		sound2ImageMap.put(R.raw.bass_drum, R.drawable.bass_drum);
		sound2ImageMap.put(R.raw.crash, R.drawable.crash);
		sound2ImageMap.put(R.raw.hihat_closed, R.drawable.hihat_closed);
		sound2ImageMap.put(R.raw.hihat_opened, R.drawable.hihat_opened);
		sound2ImageMap.put(R.raw.tom_left, R.drawable.tom_left);
		sound2ImageMap.put(R.raw.tom_right, R.drawable.tom_right);
		sound2ImageMap.put(R.raw.sticks, R.drawable.sticks);
		sound2ImageMap.put(R.raw.snare, R.drawable.snare);
	}

}
