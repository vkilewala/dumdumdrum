package appstore.massiv.com.dumdumdrum;

import java.nio.ByteBuffer;
import java.util.HashMap;

public class Utils {
	
	public static byte[] floats2bytes(float[] fs){
		ByteBuffer res = ByteBuffer.allocate(24);
		res.putFloat(fs[0]);
		res.putFloat(8,fs[1]);
		res.putFloat(16,fs[2]);
		return res.array();
	}
	
	public static float[] bytes2floats(byte[] bs){
		float[] fs = new float[3];
		ByteBuffer bb = ByteBuffer.wrap(bs);
		fs[0] = bb.getFloat();
		fs[1] = bb.getFloat(8);
		fs[2] = bb.getFloat(16);
		return fs;
	}
	
	public static String parameter2string(int[] ids){
		return ids[0] +","+ ids[1];
	}
	
	public static int[] string2parameter(String s){
		String[] tmp = s.split(",");
		return new int[]{Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1])};
	}
}
