package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerWocketDiscover extends Activity {

	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private SensorEventListener sensorListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_wocket_discover);
		// ShakeDetector initialization
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorListener = new SensorEventListener(){ 

			
			@Override
			public void onSensorChanged(SensorEvent event) {
//				WocketDrummerNewGame.wdBluetoothService.write(Utils.floats2bytes(event.values));
			}

			@Override
			public void onAccuracyChanged(Sensor arg0, int arg1) {
				// Unused 
			}
		};
	}

	@Override
	protected void onResume() {
		super.onResume();
		makeDiscoverable();
		mSensorManager.registerListener(sensorListener, mAccelerometer,
				SensorManager.SENSOR_DELAY_FASTEST);
	}
	
	private void makeDiscoverable() {
		if (BluetoothAdapter.getDefaultAdapter().getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	
	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(sensorListener);
	}
	
	public void disconnect(View view){
//		WocketDrummerNewGame.wdBluetoothService.stop();
		finish();
	}

	@Override
    public void onBackPressed() { 
    }

}
