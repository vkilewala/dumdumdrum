package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class WocketDrummerHelpScreenThree extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_help_screen_three);
	}
	
	public void playVideo(View view){
		String url = null;
		Intent i = new Intent(Intent.ACTION_VIEW);
		switch(view.getId()){
		case R.id.wd_rudiments:
			url = new String("http://www.youtube.com/watch?v=LN8UWHvoELs");
			break;
		case R.id.wd_mistakes:
			url = new String("http://www.youtube.com/watch?v=fbB3TgmKj-E");
			break;		
		case R.id.wd_exercise:
			url = new String("http://www.youtube.com/watch?v=HACkkdv4-SU");
			break;		
		}
		i.setData(Uri.parse(url));
		startActivity(i);
	}
	
	public void goBack(View view){
		Intent i = new Intent(this, WocketDrummerHelpScreenTwo.class);
		startActivity(i);
		finish();
	}
	public void goNext(View view){
		Intent i = new Intent(this, WocketDrummerNewGame.class);
		startActivity(i);
		finish();
	}


}
