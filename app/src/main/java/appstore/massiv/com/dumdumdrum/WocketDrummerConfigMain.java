package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WocketDrummerConfigMain extends Activity {

	private int[][] wocket2Sounds = new int[4][2];
	private int[][] btnID2wocketID = new int[8][2];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_wocket_drummer_config_main);
		initBtnID2wocketID();
	}
	@Override
	protected  void onResume(){
		super.onResume();
		
		drawInstruImages();
	}
	private void initBtnID2wocketID(){
		btnID2wocketID[0] = new int[]{R.id.wocket_drummer_config_main_left_ankle_1, WocketDrummerConfig.LEFT_ANKLE};
		btnID2wocketID[1] = new int[]{R.id.wocket_drummer_config_main_left_ankle_2, WocketDrummerConfig.LEFT_ANKLE};
		btnID2wocketID[2] = new int[]{R.id.wocket_drummer_config_main_right_ankle_1, WocketDrummerConfig.RIGHT_ANKLE};
		btnID2wocketID[3] = new int[]{R.id.wocket_drummer_config_main_right_ankle_2, WocketDrummerConfig.RIGHT_ANKLE};
		btnID2wocketID[4] = new int[]{R.id.wocket_drummer_config_main_left_wrist_1, WocketDrummerConfig.LEFT_WRIST};
		btnID2wocketID[5] = new int[]{R.id.wocket_drummer_config_main_left_wrist_2, WocketDrummerConfig.LEFT_WRIST};
		btnID2wocketID[6] = new int[]{R.id.wocket_drummer_config_main_right_wrist_1, WocketDrummerConfig.RIGHT_WRIST};
		btnID2wocketID[7] = new int[]{R.id.wocket_drummer_config_main_right_wrist_2, WocketDrummerConfig.RIGHT_WRIST};
	}
	
	private void getWocketSoundBinding(){
		for(int i = 0; i < 4; i++){
			String defaultBinding = Utils.parameter2string(WocketDrummerConfig.DEFAULT_WOCKET2SOUNDS[i]);
			String soundsStr = getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).getString(String.valueOf(i),defaultBinding);
			wocket2Sounds[i] = Utils.string2parameter(soundsStr);
		}
	}
	
	private void drawInstruImages(){
		getWocketSoundBinding();
		boolean flag = true;
		int[] sounds = wocket2Sounds[btnID2wocketID[0][1]];
		for(int[] btn2wocket: btnID2wocketID){
			//Button btn = (Button)findViewById(R.id.wocket_drummer_config_main_left_ankle_1);
			Button btn = (Button)findViewById(btn2wocket[0]);
			if (flag){
				sounds = wocket2Sounds[btn2wocket[1]];
			}
			int index = flag ? 0 : 1;
			int imageID = WocketDrummerConfig.sound2ImageMap.get(sounds[index]);
			Drawable image = getResources().getDrawable(imageID);
			btn.setBackgroundDrawable(image);
			flag = !flag;
		}
	}
	
	private int getWocketID(int[][] btnID2wocketID, int btnID){
		for(int[] map : btnID2wocketID){
			if (btnID == map[0]){
				return map[1];
			}
		}
		return -1;
	}
	
	public void startShakeSelect(View view){
		Button btn = (Button)view;
		Intent shakeSelectIntent = new Intent(this, WocketDrummerConfigShakeSelect.class);
		shakeSelectIntent.putExtra(WocketDrummerConfig.SELECTED_WOCKET, getWocketID(btnID2wocketID, btn.getId()));
		startActivity(shakeSelectIntent);
	}
	
	public void setDefault(View v){
		getSharedPreferences(WocketDrummerConfig.SHARE_PREFERENCE_NAME, MODE_PRIVATE).edit().clear().commit();
		drawInstruImages();
	}
	
	public void goBack(View v){
		finish();
	}
}
