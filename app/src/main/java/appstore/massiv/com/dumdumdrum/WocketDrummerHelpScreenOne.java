package appstore.massiv.com.dumdumdrum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

public class WocketDrummerHelpScreenOne extends Activity {

	VideoView vview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wocket_drummer_help_screen_one);
	}

	
	public void playVideo(View view){
		String url = null;
		Intent i = new Intent(Intent.ACTION_VIEW);
		switch(view.getId()){
		case R.id.wd_posture:
			url = new String("http://www.youtube.com/watch?v=n9UwsqIJMjs");
			break;
		case R.id.wd_terms:
			url = new String("http://www.youtube.com/watch?v=uPr2rhq3WZA");
			break;		
		}
		i.setData(Uri.parse(url));
		startActivity(i);
	}
	
	public void goBack(View view){
		Intent i = new Intent(this, WocketDrummerShowHelp.class);
		startActivity(i);
		finish();
	}
	public void goNext(View view){
		Intent i = new Intent(this, WocketDrummerHelpScreenTwo.class);
		startActivity(i);
		finish();
	}
}
